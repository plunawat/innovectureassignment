/**
 * 
 */
package com.innovecture.forecast;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"classpath*:test-servlet-context.xml"})
public class ForecastControllerTest {

	public ForecastControllerTest() { }

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before 
	public void setup() { 
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build(); 
	}

	@Test
	public void test_toGetWeatherDetails_success() throws Exception {
		String zipcode = "85001";
		mockMvc.perform(get("/get-data?zipcode=" + zipcode))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.status").value("Success"));
	}
	
	@Test
	public void test_toGetWeatherDetails_error() throws Exception {
		String zipcode = "411008";
		mockMvc.perform(get("/get-data?zipcode=" + zipcode))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.status").value("Error"));
	}
	
	@Test
	public void test_toGetWeatherDetails_emptyInput() throws Exception {
		String zipcode = "";
		mockMvc.perform(get("/get-data?zipcode=" + zipcode))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.status").value("Error"));
	}
	
	@Test
	public void test_toGetWeatherDetails_validateObject() throws Exception {
		
		String zipcode = "12345";
		mockMvc.perform(get("/get-data?zipcode=" + zipcode))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.responseObject.hourlyList[0].time").value("0"));
	}

}
