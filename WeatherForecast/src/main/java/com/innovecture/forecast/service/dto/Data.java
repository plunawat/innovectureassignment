package com.innovecture.forecast.service.dto;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Data
{
    private List<Weather> weather;

    public List<Weather> getWeather ()
    {
        return weather;
    }

    public void setWeather (List<Weather> weather)
    {
        this.weather = weather;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ weather = "+weather+"]";
    }
}

