package com.innovecture.forecast.service.dto;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {
	
	    private List<Hourly> hourly;

	    public List<Hourly> getHourly ()
	    {
	        return hourly;
	    }

	    public void setHourly (List<Hourly> hourly)
	    {
	        this.hourly = hourly;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [hourly = "+hourly+"]";
	    }
}
