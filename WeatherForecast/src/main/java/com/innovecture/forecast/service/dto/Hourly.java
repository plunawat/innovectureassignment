package com.innovecture.forecast.service.dto;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Hourly {
	private String tempF;

	private String time;

	private String tempC;

	public String getTempF() {
		return tempF;
	}

	public void setTempF(String tempF) {
		this.tempF = tempF;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getTempC() {
		return tempC;
	}

	public void setTempC(String tempC) {
		this.tempC = tempC;
	}

	@Override
	public String toString() {
		return "Hourly [tempF = " + tempF + ", time = " + time + ", tempC = " + tempC + "]";
	}
}
