package com.innovecture.forecast.service.dto;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Forecast {

	    private Hourly[] hourly;

	    public Hourly[] getHourly ()
	    {
	        return hourly;
	    }

	    public void setHourly (Hourly[] hourly)
	    {
	        this.hourly = hourly;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [hourly = "+hourly+"]";
	    }
}
