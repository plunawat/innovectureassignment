package com.innovecture.forecast.service.dto;

public class Zipcode {

	private String zip;

	private String country;

	private String city;

	private String timezone;

	private String state;

	public String getZip ()
	{
		return zip;
	}

	public void setZip (String zip)
	{
		this.zip = zip;
	}

	public String getCountry ()
	{
		return country;
	}

	public void setCountry (String country)
	{
		this.country = country;
	}

	public String getCity ()
	{
		return city;
	}

	public void setCity (String city)
	{
		this.city = city;
	}

	public String getTimezone ()
	{
		return timezone;
	}

	public void setTimezone (String timezone)
	{
		this.timezone = timezone;
	}

	public String getState ()
	{
		return state;
	}

	public void setState (String state)
	{
		this.state = state;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [zip = "+zip+", country = "+country+", city = "+city+", timezone = "+timezone+", state = "+state+"]";
	}
}
