package com.innovecture.forecast.service.impl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.innovecture.forecast.service.ForecastService;
import com.innovecture.forecast.service.dto.ForecastDetailResponse;
import com.innovecture.forecast.service.dto.Hourly;
import com.innovecture.forecast.service.dto.WeatherData;
import com.innovecture.forecast.service.dto.WeatherForecastResponse;
import com.innovecture.forecast.service.dto.Zip;

@Service
public class ForecastServiceImpl implements ForecastService {

	/**
	 * This method gets weather forecast based on the zipcode provided as input
	 * @param zipcode - any valid US zipcode (as received in controller)
	 * @return WeatherForecastResponse - response constructed based on inputs given
	 * */
	@Override
	public WeatherForecastResponse getForecastByZipcode(String zipcode) {
		WeatherForecastResponse weatherForecastResponse;
		List<Hourly> hourlyList;
		String status, message, zipCurrentDateStr;
		try {
			zipCurrentDateStr = getNextDateByZip(zipcode);
			if(null != zipCurrentDateStr && !zipCurrentDateStr.isEmpty()) {
				hourlyList = getForecast(zipcode, zipCurrentDateStr);
				if(null != hourlyList && !hourlyList.isEmpty()) {
					//min-temp
					Hourly minHourDetails = Collections.min(hourlyList, Comparator.comparing(s -> s.getTempC()));
					status = "Success";
					message = "Your request processed successfuly";
					weatherForecastResponse = buildResponseObject(hourlyList, minHourDetails, status, message);
				} else {
					status = "Error";
					message = "No data found, please give valid inputs";
					weatherForecastResponse = buildResponseObject(hourlyList, null, status, message);
				}
			} else {
				status = "Error";
				message = "Error processing your request, please try again";
				weatherForecastResponse = buildResponseObject(null, null, status, message);
			}
		} catch (IOException | ParseException e) {
			status = "Error";
			message = "Failed to process your request, please try again - " + e.getMessage();
			weatherForecastResponse = buildResponseObject(null, null, status, message);
		} catch (Exception e) {
			status = "Error";
			message = "Failed to process your request, please try again - " + e.getMessage();
			weatherForecastResponse = buildResponseObject(null, null, status, message);
		}
		return weatherForecastResponse;
	}

	/**
	 * This method is used to get weather forecast by zip-code and zipNextDate
	 * @param zipcode - zipcode provided by user as input
	 * @param zipNextDateStr - tomorrow's date in string format
	 * @return List<Hourly> - weather forecast for next 24 hours based on the inputs given
	 * */
	public List<Hourly> getForecast(String zipcode, String zipNextDateStr) throws JsonParseException, JsonMappingException, IOException
	{
		List<Hourly> hourlyList = null;
		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		WeatherData forecast;
		String weatherAPIURL, result;
		weatherAPIURL = "http://api.worldweatheronline.com/premium/v1/weather.ashx?key=ae589effe43a4e1493293535191503&q="+ zipcode +"&date="+zipNextDateStr+"&tp=1&format=json";
		result = restTemplate.getForObject(weatherAPIURL, String.class);
		forecast = mapper.readValue(result,WeatherData.class);
		hourlyList = forecast.getData().getWeather().get(0).getHourly();

		return hourlyList;
	}


	/**
	 * This method is used to get current date by zip-code
	 * @param zipcode - zipcode provided by user as input
	 * @return String - next day date in string format
	 * */
	public String getNextDateByZip(String zipcode) throws JsonParseException, JsonMappingException, IOException, ParseException{
		Date zipCurrentDate;
		String zipCurrentDateStr = null;

		String uri = "https://zipgenius.herokuapp.com/api/zipcodes/"+zipcode;
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(uri, String.class);
		ObjectMapper mapper = new ObjectMapper();

		Zip zip = mapper.readValue(result, Zip.class);

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		LocalDateTime localNow = LocalDateTime
				.now(TimeZone.getTimeZone(zip.getZipcode().getTimezone()).toZoneId());
		zipCurrentDate = format.parse(localNow.plusDays(1).toString());
		zipCurrentDateStr = format.format(zipCurrentDate).toString();
		return zipCurrentDateStr;
	} 

	/**
	 * This method is used to build the response object
	 * @param hourlyList - list of objects containing the temperature forecast details for next day(24 hrs details)
	 * @param minHourDetails - object containing the minimum temperature details 
	 * @param status - either Success or Error
	 * @param message - message to understand the request status (error/success)
	 * @return WeatherForecastResponse - response constructed based on inputs given
	 * */
	private WeatherForecastResponse buildResponseObject(List<Hourly> hourlyList, Hourly minHourDetails, String status,
			String message) {
		WeatherForecastResponse weatherForecastResponse = new WeatherForecastResponse();
		weatherForecastResponse.setStatus(status);
		weatherForecastResponse.setMessage(message);

		if(status.equalsIgnoreCase("Success")) {
			weatherForecastResponse.setCode(200);

			ForecastDetailResponse forecastDetailResponse = new ForecastDetailResponse();
			forecastDetailResponse.setHourlyList(hourlyList);
			forecastDetailResponse.setMinTempC(minHourDetails.getTempC());
			forecastDetailResponse.setMinTempF(minHourDetails.getTempF());
			forecastDetailResponse.setMinTempHour(minHourDetails.getTime());
			weatherForecastResponse.setResponseObject(forecastDetailResponse);
		} else {
			weatherForecastResponse.setCode(999);
		}
		return weatherForecastResponse;
	}
}
