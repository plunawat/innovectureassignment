package com.innovecture.forecast.service.dto;

public class Zip {

	private Zipcode zipcode;

	public Zipcode getZipcode ()
	{
		return zipcode;
	}

	public void setZipcode (Zipcode zipcode)
	{
		this.zipcode = zipcode;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [zipcode = "+zipcode+"]";
	}
}
