package com.innovecture.forecast.service.dto;

public class WeatherForecastResponse {
	
	private Integer code;
	
	private String status;
	
	private String message;
	
	private ForecastDetailResponse responseObject;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ForecastDetailResponse getResponseObject() {
		return responseObject;
	}

	public void setResponseObject(ForecastDetailResponse responseObject) {
		this.responseObject = responseObject;
	}

	@Override
	public String toString() {
		return "WeatherForecastResponse [code=" + code + ", status=" + status + ", message=" + message
				+ ", responseObject=" + responseObject + "]";
	}
	
}
