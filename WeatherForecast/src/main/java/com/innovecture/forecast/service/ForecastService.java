package com.innovecture.forecast.service;

import com.innovecture.forecast.service.dto.WeatherForecastResponse;

public interface ForecastService {
	
	WeatherForecastResponse getForecastByZipcode(String zipcode);

}
