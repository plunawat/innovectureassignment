package com.innovecture.forecast.service.dto;

import java.util.List;

public class ForecastDetailResponse {

	private String minTempC;
	
	private String minTempF;
	
	private String minTempHour;
	
	private List<Hourly> hourlyList;

	public String getMinTempC() {
		return minTempC;
	}

	public void setMinTempC(String minTempC) {
		this.minTempC = minTempC;
	}

	public String getMinTempF() {
		return minTempF;
	}

	public void setMinTempF(String minTempF) {
		this.minTempF = minTempF;
	}

	public String getMinTempHour() {
		return minTempHour;
	}

	public void setMinTempHour(String minTempHour) {
		this.minTempHour = minTempHour;
	}

	public List<Hourly> getHourlyList() {
		return hourlyList;
	}

	public void setHourlyList(List<Hourly> hourlyList) {
		this.hourlyList = hourlyList;
	}

	@Override
	public String toString() {
		return "ForecastDetailResponse [minTempC=" + minTempC + ", minTempF=" + minTempF + ", minTempHour=" + minTempHour + ", hourlyList="
				+ hourlyList + "]";
	}
	
}
