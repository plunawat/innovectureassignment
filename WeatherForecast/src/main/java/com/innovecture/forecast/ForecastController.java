package com.innovecture.forecast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.innovecture.forecast.service.ForecastService;
import com.innovecture.forecast.service.dto.WeatherForecastResponse;

@RestController
public class ForecastController {

	private static final Logger logger = LoggerFactory.getLogger(ForecastController.class);
	
	@Autowired
	private ForecastService forecastService;
	
	/**
	 * This API get weather forecast based on the zipcode provided as input
	 * @param zipcode - any valid US zipcode
	 * @return WeatherForecastResponse - response constructed based on inputs given
	 * */
	@RequestMapping(value="/get-data", method = RequestMethod.GET)
	public WeatherForecastResponse getWeatherData(@RequestParam(required=true) String zipcode) {
		logger.info("API call received getWeatherData zipcode - " + zipcode);
		WeatherForecastResponse response = forecastService.getForecastByZipcode(zipcode);
		return response;
	}
}
